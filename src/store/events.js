import axios from 'axios'

export default {
state: {
    events: []
  },
  mutations: {
    getEvents(state) {
        var array = [];
        axios.get('api/eventmanagement/v3/events/')
        .then(function (res) {
          array = res.data._embedded.events
            for(var i=0;i<array.length;i++) {
                if( array[i].entityId == "ed68e42a946441bc8b2a2a54ebb174b2") {
                     state.events.push(array[i]);
                }
            }
            console.log(state.events);
        })
        .catch(function (err) {
          console.log(err);
          console.log('Vuex get events error.');
        });
    }
},
getters: {
    events: state => state.events
  }
}