import Vue from 'vue';
import Vuex from 'vuex';

import layout from './layout';
import events from './events';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    layout,
    events
  },
});
