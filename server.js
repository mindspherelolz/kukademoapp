var express = require('express');
var cfenv = require('cfenv');
var serveStatic = require('serve-static');
appEnv = cfenv.getAppEnv();
app = express();

app.use(express.static(__dirname + "/dist"));
// app.use(serveStatic(__dirname + "/dist"));

app.listen(appEnv.port, appEnv.bind, function() {
    console.log("server starting on " + appEnv.url)
  });

// var port = process.env.PORT || 8080;
// app.listen(port);
//   console.log('server started '+ port);!Lcd!1998